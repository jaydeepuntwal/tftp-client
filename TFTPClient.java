//******************************************************************************
//
// File:    TFTPClient.java
// 
// A TFTP Client to get files from a TFTP Server
//
//******************************************************************************

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class TFTPClient implements connect, get and help for TFTP
 * 
 * @author Jaydeep Untwal (jmu8722@cs.rit.edu)
 * @version 18-Sep-2014
 */
public class TFTPClient {

	// Host Information
	String hostName;
	InetAddress hostAddress;
	int sTID = 69;

	/**
	 * Class TFTPPacket is to create and send RRQ, ACK packets and read DATA and
	 * ERR packets
	 * 
	 * @author Jaydeep Untwal (jmu8722@cs.rit.edu)
	 * @version 18-Sep-2014
	 */
	class TFTPPacket {

		/*
		 * Create RRQPacket
		 * 
		 * @param fileName - Filename to be requested
		 * 
		 * @param mode - Mode of transfer
		 * 
		 * @return RRQ byte array
		 */
		public byte[] createRRQPacket(String fileName, String mode) {

			// RRQ byte array
			byte[] packet = new byte[2 + fileName.getBytes().length + 1
					+ mode.getBytes().length + 1];

			byte[] fileNameBytes = fileName.getBytes();
			byte[] modeBytes = mode.getBytes();

			// Opcode
			byte[] opcode = { 0, 1 };

			byte zero = (byte) 0;

			int ptr = 0;

			for (int i = 0; i < opcode.length; i++) {
				packet[ptr++] = opcode[i];
			}

			// Filename
			for (int i = 0; i < fileNameBytes.length; i++) {
				packet[ptr++] = fileNameBytes[i];
			}

			// Zero
			packet[ptr++] = zero;

			// Mode
			for (int i = 0; i < modeBytes.length; i++) {
				packet[ptr++] = modeBytes[i];
			}

			// Zero
			packet[ptr++] = zero;

			return packet;
		}

		/*
		 * Create ACK packet
		 * 
		 * @param a - Block Number Byte 0
		 * 
		 * @param b - Block Number Byte 1
		 * 
		 * @return ACK byte array
		 */
		public byte[] createACKPacket(byte a, byte b) {

			byte[] packet = new byte[4];

			byte[] opcode = { 0, 4 };

			packet[0] = opcode[0];
			packet[1] = opcode[1];
			packet[2] = a;
			packet[3] = b;

			return packet;
		}

		/*
		 * Read ERR packet
		 * 
		 * @param responseBytes - Response Packet byte array
		 * 
		 * @param length - length of data
		 */
		public void readERRPacket(byte[] responseBytes, int length) {

			int code = responseBytes[3];

			byte[] msgBytes = new byte[length - 5];
			int msgPtr = 0;
			for (int i = 4; i < length - 1; i++) {
				msgBytes[msgPtr++] = responseBytes[i];
			}

			String message = new String(msgBytes);
			System.out.println("Error Code " + code + ": " + message);
		}
	}

	/*
	 * Method to connect to a host
	 * 
	 * @param hostName - Host Name
	 * 
	 * @return true if connected
	 */
	private boolean connect(String hostName) {

		try {
			// Get IP Adress
			InetAddress inetAddress = InetAddress.getByName(hostName);

			// Check if reachable
			if (inetAddress.isReachable(5000)) {

				// Print connected
				if (this.hostName == null) {
					System.out.println("Connected to " + inetAddress);
				}

				// Set hostname
				this.hostName = hostName;
				this.hostAddress = inetAddress;

				return true;
			} else {
				System.out.println(hostName + " : unknown host");
			}
		} catch (Exception e) {
			System.out.println(hostName + " : unknown host");
		}

		return false;

	}

	/*
	 * Get method to receive files from host
	 * 
	 * @param hostName - Host Name
	 * 
	 * @param source - Source File Name
	 * 
	 * @param destination - Destination File Name
	 * 
	 * @throws IOException
	 */
	private void get(String hostName, String source, String destination)
			throws IOException {
		if (connect(hostName)) {

			// Send RRQ

			byte[] rrq = new TFTPPacket().createRRQPacket(source, "octet");
			DatagramSocket clientSocket = new DatagramSocket();
			clientSocket.setSoTimeout(5000);
			DatagramPacket rrqPacket = new DatagramPacket(rrq, rrq.length,
					hostAddress, sTID);

			double startTime = System.currentTimeMillis();

			clientSocket.send(rrqPacket);

			// Get Response

			ArrayList<Byte> data = new ArrayList<Byte>();

			while (true) {

				byte[] response = new byte[516];

				DatagramPacket responsePacket = new DatagramPacket(response,
						response.length);

				// Check timeout
				try {
					clientSocket.receive(responsePacket);
				} catch (SocketTimeoutException e) {
					sTID = 69;
					System.out.println("Transfer Timed Out.");
					break;
				}

				int responseLength = responsePacket.getLength();

				byte[] responseBytes = responsePacket.getData();

				int responseOpcode = responseBytes[1];

				// Switch to Data / ERR
				switch (responseOpcode) {
				// If DATA
				case 3:

					byte[] bN = new byte[2];
					bN[0] = responseBytes[2];
					bN[1] = responseBytes[3];

					for (int i = 4; i < responseLength; i++) {
						data.add(responseBytes[i]);
					}

					sTID = responsePacket.getPort();

					// Send ACK

					byte[] ack = new TFTPPacket().createACKPacket(bN[0], bN[1]);
					DatagramPacket ackPacket = new DatagramPacket(ack,
							ack.length, hostAddress, sTID);
					clientSocket.send(ackPacket);

					break;

				// If Error
				case 5:
					TFTPPacket errPacket = new TFTPPacket();
					errPacket.readERRPacket(responseBytes, responseLength);
					break;
				}

				// Transfer Complete
				if (responseLength < 512) {

					// Reset Server Port
					sTID = 69;

					byte[] result = new byte[data.size()];

					for (int i = 0; i < data.size(); i++) {
						result[i] = data.get(i);
					}

					// Save File
					FileOutputStream fos = new FileOutputStream(destination);
					fos.write(result);
					fos.close();

					double endTime = (System.currentTimeMillis() - startTime) / 1000;

					// Print time taken
					System.out.println("Received " + data.size() + " bytes in "
							+ endTime + " s.");

					// Close Socket
					clientSocket.close();
					break;

				}

			}

		}
	}

	/*
	 * Print help information
	 */
	private void help() {
		System.out.println("Commands may be abbreviated.  Commands are:\n");
		System.out.println("connect\t\tconnect to remote tftp");
		System.out.println("get\t\treceive file");
		System.out.println("quit\t\texit tftp");
		System.out.println("?\t\tprint help information");
	}

	/*
	 * Main
	 */
	public static void main(String args[]) {

		try {

			TFTPClient tc = new TFTPClient();

			Scanner scan = new Scanner(System.in);

			// Get Input
			while (true) {
				System.out.print("tftp> ");
				String input = scan.nextLine();

				String commands[] = input.split(" ");

				switch (commands[0]) {

				case "connect":

					if (commands.length > 1) {
						tc.connect(commands[1]);
					} else {
						System.out.println("Usage:\nconnect [Host Name]");
					}

					break;

				case "quit":
					scan.close();
					System.exit(0);
					break;

				case "get":
					if (tc.hostName != null && commands.length == 2) {
						tc.get(tc.hostName, commands[1], commands[1]);
					} else if (tc.hostName != null && commands.length == 3) {
						tc.get(tc.hostName, commands[1], commands[2]);
					} else if (commands.length == 3) {
						tc.get(commands[1], commands[2], commands[2]);
					} else if (commands.length == 4) {
						tc.get(commands[1], commands[2], commands[3]);
					} else {
						System.out
								.println("Usage:\nIf connected to host\nget [Source] [Destination]\nElse\nget [Host Name] [Source] [Destination]");
					}
					break;

				case "?":
					tc.help();
					break;

				default:
					System.out.println("Invalid Command");
					break;
				}

			}

		} catch (Exception e) {
		}
	}
}
